# Gemnasium Python analyzer

Dependency Scanning for Python projects based on Gemnasium.

This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Gemnasium git submodule

This Go project depends on the [gemnasium](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium) project,
which it includes as a git submodule (see [gemnasium](gemnasium)).

This analyzer imports packages from the [gemnasium](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium) project,
in order to parse, build, and scan matching python projects.
For that reason, it's often required (e.g. in the case of adding a new python package manager) to update `gemnasium` as well as `gemnasium-python` when updating this analyzer.

For instance, changing a dependency file parser used by `gemnasium-python` involves these steps:
1. clone `gemnasium-python`, and create a branch
1. enter the `gemnasium` directory, create a branch, update and commit code, push branch, create MR in `gemnasium`
1. go back to the root directory, `git add gemnasium`, create commit, push branch, and create MR in `gemnasium-python``
1. submit both MRs, and first merge the one created in `gemnasium` when it's ready
1. enter the `gemnasium` directory, and checkout the `master` branch
1. go back to the root directory, `git add gemnasium`, create commit, and push branch
1. merge MR in `gemnasium-python`

Warning! The [gemnasium](gemnasium) file only contains a commit SHA, and it doesn't tell what branch we're tracking.
Developers can easily miss that gemnasium refers to a commit that's going to be removed
Thus it is easy to miss that the `gemnasium` sub-module refers to a commit that's going to be removed
when the submodule's branch is squashed into `master` after approval.

Warning! This Go project uses the [`replace` directive](https://golang.org/ref/mod#go-mod-file-replace)
to replace the `gemnasium/v2` dependency with the `./gemnasium` directory.
As a result, the version of `gemnasium/v2` that shows up in [go.mod](go.mod) isn't relevant.

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.
