package metadata

import (
	"fmt"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

const (
	// AnalyzerVendor is the vendor/maintainer of the analyzer
	AnalyzerVendor = "GitLab"

	analyzerID = "gemnasium-python"

	// AnalyzerName is the name of the analyzer
	AnalyzerName = analyzerID

	scannerVendor = AnalyzerVendor
	scannerURL    = "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python"

	// Type returns the type of the scan
	Type report.Category = report.CategoryDependencyScanning
)

var (
	// AnalyzerVersion is a placeholder value which the Dockerfile will dynamically
	// overwrite at build time with the most recent version from the CHANGELOG.md file
	AnalyzerVersion = "not-configured"

	// ScannerVersion is the semantic version of the scanner
	ScannerVersion = AnalyzerVersion

	// ReportScanner returns identifying information about a security scanner
	ReportScanner = report.ScannerDetails{
		ID:      analyzerID,
		Name:    AnalyzerName,
		Version: ScannerVersion,
		Vendor: report.Vendor{
			Name: scannerVendor,
		},
		URL: scannerURL,
	}

	// AnalyzerUsage provides a one line usage string for the analyzer
	AnalyzerUsage = fmt.Sprintf("%s %s analyzer v%s", AnalyzerVendor, AnalyzerName, AnalyzerVersion)
)
